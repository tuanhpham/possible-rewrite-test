import numpy as np, pandas as pd
import matplotlib.pyplot as plt

A = np.zeros((500, 500)); 
A[250:350,100:200] = 1; 
A[295:305,300:400] = 1

for xi, yi in zip(x.flatten(), y.flatten()):
	d = np.sqrt( (xi-c[1])**2 + (yi-c[0])**2 )
	if d < 45 and d > 40 and yi < c[0] -1:
		A[int(yi), int(xi)] = 1

plt.imshow(A, cmap='gray', origin='lower'); plt.show()
plt.imshow(A + 1.8*M, cmap='BuPu', origin='lower'); plt.show()

from PIL import Image

Image.fromarray(A*255).convert('RGB').save('images/e1-clean.jpg')

im = Image.fromarray((A[::-1] + 1.8*M)*200).convert('RGB')
im.save('images/e2-noise.jpg')
im.save('images/e2-noise.tif')

